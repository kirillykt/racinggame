using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data
{
    public class Identified : ScriptableObject
    {
        [SerializeField] private string _id;
        public string ID => _id;
    }
}
