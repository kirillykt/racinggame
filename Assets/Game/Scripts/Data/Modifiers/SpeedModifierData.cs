using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data.Modifiers
{
    [CreateAssetMenu(menuName = "Car Game/Modifiers/SpeedModifier")]
    public class SpeedModifierData : ModifierData
    {
        public float AdditionalMaxVelocity;
        public float AdditionalAcceleration;
    }
}