using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data.Modifiers
{
    public class ModifierData : Identified
    {
        public float Duration;
    }
}