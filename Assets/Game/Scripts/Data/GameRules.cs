using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data
{
    [CreateAssetMenu(menuName = "Car Game/Data/Game Rules")]
    public class GameRules : ScriptableObject
    {
        [SerializeField] private float _respawnTime = 3f;
        public float RespawnTime => _respawnTime;
    }
}