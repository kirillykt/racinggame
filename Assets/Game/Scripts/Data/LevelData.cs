using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data
{
    [CreateAssetMenu(menuName = "Car Game/Data/LevelData")]
    public class LevelData : ScriptableObject
    {
        [SerializeField] private float _preStartTime = 1f;
        [SerializeField] private float _countdownTime = 3f;
        [SerializeField] private CarModelData[] _botsData = null;
        public float PreStartTime => _preStartTime;
        public float CountdownTime => _countdownTime;
        public CarModelData[] BotsData => _botsData;
    }
}