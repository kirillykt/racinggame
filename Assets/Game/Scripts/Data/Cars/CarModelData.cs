using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace CG.Data
{
    [CreateAssetMenu(menuName = "Car Game/Data/Car Data")]
    public class CarModelData : Identified
    {
        [SerializeField] private float _maxVelocity;
        [SerializeField] private float _accelerationForce;
        [SerializeField] private float _maxRotationSpeed;
        [SerializeField] private float _rotationAccelerationSpeed = 90f;
        [SerializeField] private float _maxBackwardsVelocity;
        [SerializeField] private float _backwardsAccelerationForce;
        [SerializeField] private int _fullHP;
        [SerializeField] private CarAbilityData _mainAbility;
        [SerializeField] private CarAbilityData _secondAbility;

        public float MaxVelocity => _maxVelocity;
        public float AccelerationForce => _accelerationForce;
        public float MaxRotationSpeed => _maxRotationSpeed;
        public float RotationAccelerationSpeed => _rotationAccelerationSpeed;
        public float MaxBackwardsVelocity => _maxBackwardsVelocity;
        public float BackwardsAccelerationForce => _backwardsAccelerationForce;
        public int FullHP => _fullHP;
        public CarAbilityData MainAbility => _mainAbility;
        public CarAbilityData SecondAbility => _secondAbility;
    }
}