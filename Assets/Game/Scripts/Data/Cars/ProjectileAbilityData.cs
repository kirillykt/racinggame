using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;
using UnityEngine.Serialization;

namespace CG.Data
{
    [CreateAssetMenu(menuName = "Car Game/Ability/ProjectileAbilityData")]
    public class ProjectileAbilityData : CarAbilityData
    {
        [SerializeField] private int _damage;
        [FormerlySerializedAs("_force")] [SerializeField] private float _projectilePushForce;
        [SerializeField] private float _damageRadius;
        [SerializeField] private float _explosionForce;
        [SerializeField] private LayerMask _destroyMask;
        [SerializeField] private float _torque;
        public int Damage => _damage;
        public float ProjectilePushForce => _projectilePushForce;
        public float DamageRadius => _damageRadius;
        public float ExplosionForce => _explosionForce;
        public LayerMask DestroyMask => _destroyMask;
        public float Torque => _torque;
    }
}