using System.Collections;
using System.Collections.Generic;
using CG.Data.Modifiers;
using UnityEngine;

namespace CG.Data
{
    [CreateAssetMenu(menuName = "Car Game/Ability/ModifierAbility")]
    public class ModifierAbilityData : CarAbilityData
    {
        public ModifierData ModifierData;
    }
}