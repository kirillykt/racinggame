using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data
{
    public class CarAbilityData : Identified
    {
        [SerializeField] private float _cooldown = 0f;
        public float Cooldown => _cooldown;
    }
}