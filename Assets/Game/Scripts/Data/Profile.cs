using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data
{
    [Serializable]
    public class Profile
    {
        public string SelectedCarID = "racing";
        public string Nickname = "Player";
    }
}