using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data.Localization
{
    public class Localization : MonoBehaviour
    {
        [SerializeField] private string _fileLocation = "Localization/";

        private string defaultLocalization = "en";
        private Dictionary<string, string> _localizationDictionary = new Dictionary<string, string>();
        public static Localization Instance { get; private set; }
        private void Start()
        {
            
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            _fileLocation += defaultLocalization;
            var file = Resources.Load<TextAsset>(_fileLocation);
            var lines = file.text.Split(new [] { '\r', '\n' });//Split by new lines
            foreach (var line in lines)
            {
                var results = line.Split(':');
                _localizationDictionary.Add(results[0], results[1]);
            }
        }

        public string GetText(string key)
        {
            return _localizationDictionary.ContainsKey(key) ? _localizationDictionary[key] : key;
        }
    }
}