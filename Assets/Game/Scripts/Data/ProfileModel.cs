﻿using System;
using System.IO;
using System.Text;
using CG.Data;
using UnityEngine;

namespace CG.Data
{
    public class ProfileModel : MonoBehaviour
    {
        public static ProfileModel Instance { get; private set; }
        public Profile Profile { get; private set; }

        private string saveFilePath;
        private void Awake()
        {
            saveFilePath = Application.persistentDataPath + "/save.json";
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            LoadOrCreateProfile();
        }

        private void LoadOrCreateProfile()
        {
            if (!File.Exists(saveFilePath))
            {
                Profile = new Profile();
                SaveProfileToFile();
            }
            else
            {
                var json = File.ReadAllText(saveFilePath);
                Debug.Log("Loading profile: " + json);
                Profile = JsonUtility.FromJson<Profile>(json);
            }
            Debug.Log("SaveFile: " + saveFilePath);
        }

        private void SaveProfileToFile()
        {
            
            var json = JsonUtility.ToJson(Profile);
            Debug.Log(json);
            File.WriteAllText(saveFilePath, json);
        }

        public void SetSelectedCar(string id)
        {
            Profile.SelectedCarID = id;
            SaveProfileToFile();
        }
    }
}