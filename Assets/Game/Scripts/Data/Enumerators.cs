using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Data
{
    public enum GameState {PreStart, Start, InProgress, PreFinish, Finish}
}