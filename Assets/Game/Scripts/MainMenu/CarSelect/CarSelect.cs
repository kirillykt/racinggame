using System;
using CG.Data;
using UnityEngine;

namespace CG.MainMenu
{
    public class CarSelect : MonoBehaviour
    {
        [SerializeField] private Identity[] _cars = null;
        private int _currentCarIndex = 0;
        public event Action OnSelectButton;
        private void Start()
        {
            for (int i = 0; i < _cars.Length; i++)
            {
                var car = _cars[i];
                var id = ProfileModel.Instance.Profile.SelectedCarID;
                var isPlayerCar = car.ID.Equals(id);
                car.gameObject.SetActive(isPlayerCar);
                _currentCarIndex = isPlayerCar ? i : _currentCarIndex;
            }
        }

        public void OnSelectButtonClicked()
        {
            var id = _cars[_currentCarIndex].GetComponent<Identity>().ID;
            ProfileModel.Instance.SetSelectedCar(id);
            OnSelectButton.Invoke();
        }

        public void OnNextButtonClicked()
        {
            if (_currentCarIndex == _cars.Length - 1)
            {
                return;
            }
            _cars[_currentCarIndex].gameObject.SetActive(false);
            _cars[++_currentCarIndex].gameObject.SetActive(true);
        }

        public void OnPreviousButtonClicked()
        {
            if (_currentCarIndex <= 0)
            {
                return;
            }
            _cars[_currentCarIndex].gameObject.SetActive(false);
            _cars[--_currentCarIndex].gameObject.SetActive(true);
        }
    }
}