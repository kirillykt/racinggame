using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace CG.MainMenu.UI
{
    public class PlayButton : MonoBehaviour
    {
        [FormerlySerializedAs("_mapSelectionGroup")] [SerializeField] private MapSelect mapSelect = null;
        private void Start()
        {
        }

        public void StartGame()
        {
            var index = mapSelect.MapIndex;
            SceneManager.LoadScene(index);
        }
    }
}