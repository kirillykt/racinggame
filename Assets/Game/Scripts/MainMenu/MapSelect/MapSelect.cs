using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.MainMenu.UI
{
    public class MapSelect : MonoBehaviour
    {
        [SerializeField] private List<MapSelectButton> _mapSelectButtons = new List<MapSelectButton>();
        private MapSelectButton _selected;
        public event Action<MapSelectButton> OnSelectedChanged;
        public int MapIndex => _selected.MapIndex;
        private void Start()
        {
            _selected = _mapSelectButtons[0];
            OnSelectedChanged.Invoke(_selected);
        }

        public void OnSelect(MapSelectButton button)
        {
            _selected = button;
            OnSelectedChanged.Invoke(button);
        }
    }
}