using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CG.MainMenu.UI
{
    public class MapSelectButton : MonoBehaviour
    {
        private MapSelect _mapSelect = null;
        private bool _isSelected = false;
        private float _animationTime = 0.5f;
        [SerializeField] private int _mapIndex;
        [SerializeField] private AnimationCurve _selectedScaleCurve = null;
        [SerializeField] private AnimationCurve _deselectedScaleCurve = null;
        [SerializeField] private Image _image = null;
        [SerializeField] private GameObject _text = null;

        public int MapIndex => _mapIndex;
        private void Awake()
        {
            _mapSelect = GetComponentInParent<MapSelect>();
            _mapSelect.OnSelectedChanged += OnSelectedChanged;
        }

        private void OnSelectedChanged(MapSelectButton button)
        {
            if (button != this && _isSelected)
            {
                _isSelected = false;
                StopAllCoroutines();
                StartCoroutine(OnDeselect());
            }
            else if(button == this && !_isSelected)
            {
                _isSelected = true;
                StopAllCoroutines();
                StartCoroutine(OnSelect());
            }
        }

        private IEnumerator OnDeselect()
        {
            _text.SetActive(false);
            var elapsedTime = 0f;
            while (elapsedTime < _animationTime)
            {
                elapsedTime += Time.deltaTime;
                var alpha = elapsedTime / _animationTime;
                var scale = _deselectedScaleCurve.Evaluate(alpha) * Vector3.one;
                var color = Color.Lerp(Color.red, Color.white, alpha);
                transform.localScale = scale;
                _image.color = color;
                yield return null;
            }
        }

        private IEnumerator OnSelect()
        {
            var elapsedTime = 0f;
            while (elapsedTime < _animationTime)
            {
                elapsedTime += Time.deltaTime;
                var alpha = elapsedTime / _animationTime;
                var scale = _selectedScaleCurve.Evaluate(alpha) * Vector3.one;
                var color = Color.Lerp(Color.white, Color.red, alpha);
                transform.localScale = scale;
                _image.color = color;
                yield return null;
            }
            _text.SetActive(true);
        }

        public void OnClick()
        {
            _mapSelect.OnSelect(this);
        }
    }
}