using System.Collections;
using System.Collections.Generic;
using CG.MainMenu.UI;
using UnityEngine;

namespace CG.MainMenu
{
    public class MainMenuController : MonoBehaviour
    {
        [SerializeField] private CarSelect _carSelect = null;
        [SerializeField] private MapSelect _mapSelect = null;
        private void Start()
        {
            _carSelect.OnSelectButton += OnSelectButton;
        }

        private void OnSelectButton()
        {
            _carSelect.gameObject.SetActive(false);
            _mapSelect.gameObject.SetActive(true);
        }
    }
}