using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.View
{
    public class ParticleEffect : MonoBehaviour
    {
        [SerializeField] private string _id;
        public string ID => _id;
        [HideInInspector] public bool IsFree = true;
        private void Awake()
        {
            
        }
    }
}