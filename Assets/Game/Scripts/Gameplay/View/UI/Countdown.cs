using System.Collections;
using System.Collections.Generic;
using CG.Data;
using CG.Data.Localization;
using CG.Gameplay.Model;
using DG.Tweening;
using TMPro;
using UnityEngine;

namespace CG.Gameplay.View.UI
{
    public class Countdown : MonoBehaviour
    {
        private GameSession _gameSession;
        [SerializeField] private TextMeshProUGUI _text = null;
        [SerializeField] private string _startText = null;
        private GameState _state;
        
        private void Start()
        {
            _gameSession = GameSession.Instance;
        }

        private void Update()
        {
            if (_gameSession.State == GameState.Start && _state != GameState.Start)
            {
                StartCoroutine(StartCountdown());
            }
            _state = _gameSession.State;
        }

        private IEnumerator StartCountdown()
        {
            _text.enabled = true;
            for (int i = (int)LevelSystem.Instance.LevelData.CountdownTime; i > 0; i--)
            {
                _text.text = i.ToString();
                AnimateText();
                yield return new WaitForSeconds(1f);
            }
            _text.text = Localization.Instance.GetText(_startText);
            AnimateText();
        }

        private void AnimateText()
        {
            _text.enabled = true;
            var sequence = DOTween.Sequence();
            sequence.Append(_text.transform.DOScale(3f, 0f))
                .Append(_text.transform.DOScale(0f, 0.9f))
                .OnComplete(() => _text.enabled = false);
        }
    }
}