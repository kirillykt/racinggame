using System.Collections;
using System.Collections.Generic;
using CG.Gameplay.Model;
using Cinemachine;
using UnityEngine;

namespace CG.Gameplay.View
{
    public class CinemachineController : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera = null;
        private IEnumerator Start()
        {
            yield return new WaitUntil(() => PlayerCarController.My != null);
            var target = PlayerCarController.My.transform;
            _cinemachineVirtualCamera.Follow = target;
            _cinemachineVirtualCamera.LookAt = target;
        }
    }
}