using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.View
{
    public class ParticleEffectCollection : MonoBehaviour
    {
        private string _id;
        public string ID => _id;
        private List<ParticleEffect> _effects = new List<ParticleEffect>();
        
        private void Start()
        {
            
        }

        public void Init(ParticleEffect prefab, int count)
        {
            _id = prefab.ID;
            for (int i = 0; i < count; i++)
            {
                var instance = Instantiate(prefab.gameObject, transform).GetComponent<ParticleEffect>();
                _effects.Add(instance);
            }
        }

        public void Play(Vector3 position)
        {
            var effect = OccupyFreeEffect();
            position.y = effect.transform.position.y;
            effect.transform.position = position;
            effect.GetComponent<ParticleSystem>().Play();
        }

        private ParticleEffect OccupyFreeEffect()
        {
            foreach (var effect in _effects)
            {
                if (effect.IsFree)
                {
                    effect.IsFree = false;
                    StartCoroutine(MarkFree(effect));
                    return effect;
                }
            }
            return null;
        }

        private IEnumerator MarkFree(ParticleEffect effect)
        {
            yield return new WaitForSeconds(5f);
            effect.IsFree = true;
        }
    }
}