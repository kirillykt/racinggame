using System.Collections;
using System.Collections.Generic;
using CG.Gameplay.Model;
using UnityEngine;

namespace CG.Gameplay.View
{
    public class ProjectileView : MonoBehaviour
    {
        private ProjectileModel _projectileModel;
        [SerializeField] private Material _allyMaterial = null;
        [SerializeField] private MeshRenderer _meshRenderer = null;
        private void Awake()
        {
            _projectileModel = GetComponent<ProjectileModel>();
            _projectileModel.OnDestroy += () =>
                ParticleEffectsManager.Instance.Play(_projectileModel.Data.ID, transform.position);
            _projectileModel.OnInit += OnInit;
        }

        private void OnInit(DamageAgent agent)
        {
            if (agent.Equals(CarController.My.CarComponentLinks.DamageAgent))
            {
                _meshRenderer.material = _allyMaterial;
            }
        }
    }
}