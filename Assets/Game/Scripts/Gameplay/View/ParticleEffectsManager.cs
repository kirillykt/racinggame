using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CG.Gameplay.View
{
    public class ParticleEffectsManager : MonoBehaviour
    {
        [SerializeField] private List<GameObject> _prefabs = null;
        [SerializeField] private GameObject _particleEffectCollectionPrefab = null;
        [SerializeField] private int _amountPerEffect = 8;
        private Dictionary<string, ParticleEffectCollection> _instances = new Dictionary<string, ParticleEffectCollection>();
        
        public static ParticleEffectsManager Instance { get; private set; }
        private void Start()
        {
            Instance = this;
            foreach (var prefab in _prefabs)
            {
                var collection = Instantiate(_particleEffectCollectionPrefab, transform);
                collection.transform.localPosition = Vector3.zero;
                var collectionComponent = collection.GetComponent<ParticleEffectCollection>();
                var effect = prefab.GetComponent<ParticleEffect>();
                collection.gameObject.name = effect.ID;
                _instances.Add(effect.ID, collectionComponent);
                collectionComponent.Init(effect, _amountPerEffect);
            }
        }

        public void Play(string id, Vector3 position)
        {
            _instances[id].Play(position);
        }
    }
}