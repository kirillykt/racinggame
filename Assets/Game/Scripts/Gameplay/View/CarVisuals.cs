using System.Collections;
using System.Collections.Generic;
using CG.Gameplay.Model;
using UnityEngine;

namespace CG.Gameplay.View
{
    public class CarVisuals : MonoBehaviour
    {
        private void Start()
        {
            var agent = GetComponentInParent<DamageAgent>();
            agent.OnResurrect += OnResurrect;
            agent.OnDied += OnDied;
        }

        private void OnDied()
        {
            gameObject.SetActive(false);
        }

        private void OnResurrect()
        {
            gameObject.SetActive(true);
        }
    }
}