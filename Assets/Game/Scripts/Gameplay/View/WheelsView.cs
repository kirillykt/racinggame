using System;
using System.Collections;
using System.Collections.Generic;
using CG.Data;
using CG.Gameplay.Model;
using UnityEngine;
using UnityEngine.Serialization;

namespace CG.Gameplay.View
{
    public class WheelsView : MonoBehaviour
    {
        [FormerlySerializedAs("_topRightWheel")] [SerializeField] private Transform _frontRightWheel = null;
        [FormerlySerializedAs("_topLeftWheel")] [SerializeField] private Transform _frontLeftWheel = null;
        [SerializeField] private float _maxRotationY = 45f;
        [SerializeField] private List<Transform> _wheels = new List<Transform>();

        private CarMotor _carMotor = null;
        private float _timePerRotation = 1f / 10f;
        private float _elapsedTime;
        
        private void Start()
        {
            _carMotor = GetComponentInParent<CarMotor>();
        }

        private void Update()
        {
            var desiredDirection = _carMotor.DesiredDirection;
            var x = desiredDirection.x;
            var desiredRotation = x * _maxRotationY;
            var rotationLeft = _frontLeftWheel.localEulerAngles;
            var rotationRight = _frontRightWheel.localEulerAngles;
            rotationLeft.y = desiredRotation;
            rotationRight.y = desiredRotation;
            
            _frontLeftWheel.localRotation = Quaternion.Lerp(_frontLeftWheel.localRotation, Quaternion.Euler(rotationLeft), 0.2f);
            _frontRightWheel.localRotation = Quaternion.Lerp(_frontRightWheel.localRotation, Quaternion.Euler(rotationRight), 0.2f);
            _elapsedTime += Time.deltaTime;
            var alpha = _elapsedTime / _timePerRotation;
            foreach (var wheel in _wheels)
            {
                var localEuler = wheel.localRotation.eulerAngles;
                localEuler.x = alpha * 360f;
                wheel.localRotation = Quaternion.Euler(localEuler);
            }
            if (_elapsedTime > _timePerRotation)
            {
                _elapsedTime -= _timePerRotation;
            }
        }
    }
}