using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class DamageSystem : MonoBehaviour
    {
        public static DamageSystem Instance { get; private set; }
        private List<DamageAgent> _damageAgents = new List<DamageAgent>();
        public List<DamageAgent> DamageAgents => _damageAgents;
        private void Awake()
        {
            Instance = this;
        }

        public void DealDamage(DamageAgent attacker, DamageAgent victim, int damage)
        {
            victim.ReceiveDamage(damage);
            Debug.Log("DamageSystem DealDamage Attacker: " + attacker.name + " Victim:" + victim.name + " Damage: " + damage);
        }

        public void Register(DamageAgent damageAgent)
        {
            _damageAgents.Add(damageAgent);
        }
    }
}