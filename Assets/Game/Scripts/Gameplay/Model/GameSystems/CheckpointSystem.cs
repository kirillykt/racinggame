using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class CheckpointSystem : MonoBehaviour
    {
        [SerializeField] private List<Checkpoint> _checkpoints = new List<Checkpoint>();
        [SerializeField] private int _lapsCount = 2;

        public int LastCheckpointIndex
        {
            get
            {
                var biggestIndex = 0;
                foreach (var checkpoint in _checkpoints)
                {
                    if (checkpoint.Index > biggestIndex)
                    {
                        biggestIndex = checkpoint.Index;
                    }
                }
                return biggestIndex;
            }
        }
        public static CheckpointSystem Instance { get; private set; }
        public Action<int, CarCheckpointsAgent> OnCheckpointReached;
        public Action<CarCheckpointsAgent> OnFinishReached;
        public int LapsCount => _lapsCount;
        
        private void Awake()
        {
            Instance = this;
        }

        private void Start()
        {

        }

        private void OnDestroy()
        {
            Instance = null;
        }
    }
}