using System;
using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class GameSession : MonoBehaviour
    {
        public static GameSession Instance { get; private set; }
        [SerializeField] private GameRules _gameRules = null;
        private GameState _state = GameState.PreStart;
        private LevelSystem _levelSystem;
        private float _remainPreStartTime;
        private float _remainCountdownTime;
        
        public GameRules GameRules => _gameRules;
        public GameState State => _state;
        private void Awake()
        {
            Instance = this;
            _levelSystem = LevelSystem.Instance;
            _remainPreStartTime = _levelSystem.LevelData.PreStartTime;
            _remainCountdownTime = _levelSystem.LevelData.CountdownTime;
        }

        private void Start()
        {
        }

        private void Update()
        {
            switch (_state)
            {
                case GameState.PreStart:
                    UpdatePreStart();
                    break;
                case GameState.Start:
                    UpdateStart();
                    break;
            }
        }



        private void UpdatePreStart()
        {
            if (_remainPreStartTime > 0)
            {
                _remainPreStartTime -= Time.deltaTime;
                return;
            }
            _state = GameState.Start;
        }

        private void UpdateStart()
        {
            if (_remainCountdownTime > 0)
            {
                _remainCountdownTime -= Time.deltaTime;
                return;
            }
            _state = GameState.InProgress;
        }
    }
}