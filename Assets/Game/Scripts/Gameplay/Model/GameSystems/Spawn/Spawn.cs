using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class Spawn : MonoBehaviour
    {
        [HideInInspector] public bool IsFree = true;
    }
}