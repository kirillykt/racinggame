using System.Collections;
using System.Collections.Generic;
using CG.Data;
using CG.Gameplay.Model.AI;
using CG.MainMenu;
using UnityEngine;
using Utils;

namespace CG.Gameplay.Model
{
    public class SpawnSystem : MonoBehaviour
    {
        [SerializeField] private GameObject _carPrefab = null;
        [SerializeField] private List<Spawn> _spawns = null;
        private readonly List<Spawn> _freeSpawns = new List<Spawn>();
        private void Start()
        {
            _freeSpawns.AddRange(_spawns);
            var player = SpawnCar(ProfileModel.Instance.Profile.SelectedCarID);
            player.GetComponent<PlayerCarController>().enabled = true;
            CarController.My = player.GetComponent<PlayerCarController>();
            player.gameObject.name = "Player";
            SpawnBots();
        }

        private void SpawnBots()
        {
            foreach (var data in LevelSystem.Instance.LevelData.BotsData)
            {
                var bot = SpawnCar(data.ID);
                bot.GetComponentInChildren<AICarDriver>(true).enabled = true;
                bot.GetComponentInChildren<AIMemory>(true).enabled = true;
                bot.gameObject.name = data.ID;
                var aiAbilityLogics = bot.GetComponentsInChildren<AIAbilityLogic>(true);
                foreach (var logic in aiAbilityLogics)
                {
                    logic.enabled = true;
                }
            }
        }

        private GameObject SpawnCar(string carId)
        {
            var spawn = OccupyFreeSpawn();
            var carInstance = Instantiate(_carPrefab, spawn.transform.position, spawn.transform.rotation);
            var cars = carInstance.gameObject.GetComponentsInChildren<Identity>(true);
            foreach (var car in cars)
            {
                if (car.ID.Equals(carId))
                {
                    car.gameObject.SetActive(true);
                }
            }
            return carInstance;
        }

        private Spawn OccupyFreeSpawn()
        {
            var spawn = _freeSpawns.RandomElement();
            spawn.IsFree = false;
            _freeSpawns.Remove(spawn);
            return spawn;
        }
    }
}

