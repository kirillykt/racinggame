using System;
using System.Collections;
using System.Collections.Generic;
using CG.Gameplay.Model;
using UnityEngine;

public class CarCheckpointsAgent : MonoBehaviour
{
    private int _currentCheckpoint;
    private int _currentLap;
    
    private void Start()
    {
        CheckpointSystem.Instance.OnCheckpointReached += OnCheckpointReached;
        CheckpointSystem.Instance.OnFinishReached += OnFinishReached;
    }

    private void OnFinishReached(CarCheckpointsAgent car)
    {
        if (car != this)
        {
            return;
        }

        if (CheckpointSystem.Instance.LastCheckpointIndex == _currentCheckpoint)
        {
            OnLapFinished();
        }
    }

    private void OnDestroy()
    {
        if (CheckpointSystem.Instance)
        {
            CheckpointSystem.Instance.OnCheckpointReached -= OnCheckpointReached;
        }
    }

    private void OnCheckpointReached(int index, CarCheckpointsAgent agent)
    {
        if (agent != this)
        {
            return;
        }
        if (_currentCheckpoint == index - 1)
        {
            _currentCheckpoint = index;
            Debug.Log("CurrentCheckpoint: " + _currentCheckpoint);
        }
    }

    private void OnLapFinished()
    {
        _currentLap++;
        Debug.Log("Lap finished: " + _currentLap);
        _currentCheckpoint = 0;
        if (_currentLap >= CheckpointSystem.Instance.LapsCount)
        {
            OnFinish();
        }
    }

    private void OnFinish()
    {
        Debug.Log("Finished");
    }
}
