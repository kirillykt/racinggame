using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class Finish : MonoBehaviour
    {
        private void Start()
        {

        }

        private void OnTriggerEnter(Collider other)
        {
            var car = other.GetComponentInParent<CarCheckpointsAgent>();
            if (car != null)
            {
                CheckpointSystem.Instance.OnFinishReached.Invoke(car);
            }
        }
    }
}