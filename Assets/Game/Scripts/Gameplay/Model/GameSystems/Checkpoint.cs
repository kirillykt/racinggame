using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class Checkpoint : MonoBehaviour
    {
        private CheckpointSystem _checkpointSystem;
        [SerializeField] private int _index;

        public int Index => _index;
        private void Start()
        {
            _checkpointSystem = CheckpointSystem.Instance;
        }

        private void OnTriggerEnter(Collider other)
        {
            var car = other.GetComponentInParent<CarCheckpointsAgent>();
            if (car != null)
            {
                _checkpointSystem.OnCheckpointReached.Invoke(_index, car);
            }
        }
    }
}