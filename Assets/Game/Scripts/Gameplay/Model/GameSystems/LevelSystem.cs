using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class LevelSystem : MonoBehaviour
    {
        
        public static LevelSystem Instance { get; private set; }
        [SerializeField] private LevelData _levelData = null;
        public LevelData LevelData => _levelData;
        private void Awake()
        {
            Instance = this;
        }
    }
}