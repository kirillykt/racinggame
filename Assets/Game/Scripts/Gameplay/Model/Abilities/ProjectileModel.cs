using System;
using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class ProjectileModel : MonoBehaviour
    {
        private Collider[] _colliders = new Collider[20];
        private ProjectileAbility _projectileAbility;
        private ProjectileAbilityData _data;
        private DamageAgent _owner;
        public event Action OnDestroy;
        public ProjectileAbilityData Data => _data;

        public event Action<DamageAgent> OnInit;
        private void Start()
        {
            var colliders = _owner.GetComponent<CarComponentLinks>().Colliders;
            var selfColliders = GetComponentsInChildren<Collider>();
            foreach (var col in colliders)
            {
                foreach (var selfCollider in selfColliders)
                {
                    Physics.IgnoreCollision(col, selfCollider, true);
                }
            }
        }

        public void Init(ProjectileAbility ability)
        {
            _projectileAbility = ability;
            _data = (ProjectileAbilityData)ability.Data;
            _owner = ability.Car.DamageAgent;
            OnInit.Invoke(_owner);
        }

        private void OnCollisionEnter(Collision other)
        {
            if (_data.DestroyMask != (_data.DestroyMask | (1 << other.gameObject.layer)))
            {
                return;
            }
            Debug.Log("Collision with: " + other.gameObject.name);
            var colliders = Physics.OverlapSphereNonAlloc(other.contacts[0].point, _data.DamageRadius, _colliders);
            for(int i = 0; i < colliders; i++)
            {
                var collider = _colliders[i];
                var rigidbody = collider.GetComponentInParent<Rigidbody>();
                if (rigidbody)
                {
                    if (!rigidbody.gameObject.Equals(_owner.gameObject))
                    {
                        rigidbody.AddExplosionForce(_data.ExplosionForce, other.contacts[0].point, _data.DamageRadius,
                            0.2f, ForceMode.Impulse);
                        var direction = other.collider.transform.position - other.contacts[0].point;
                        rigidbody.AddTorque(direction.normalized * _data.Torque);
                    }
                }

                var damageAgent = collider.GetComponentInParent<DamageAgent>();
                if (damageAgent)
                {
                    if (!damageAgent.Equals(_owner))
                    {
                        DamageSystem.Instance.DealDamage(_owner, damageAgent, _data.Damage);
                    }
                }
            }
            OnDestroy.Invoke();
            Destroy(gameObject);
        }
    }
}