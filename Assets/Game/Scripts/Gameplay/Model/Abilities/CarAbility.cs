using System;
using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public abstract class CarAbility : MonoBehaviour
    {
        [SerializeField] protected CarAbilityData _carAbilityData;
        public virtual bool CanUse => _currentCooldown <= 0;
        public CarAbilityData Data => _carAbilityData;
        [HideInInspector] public CarComponentLinks Car;
        protected float _currentCooldown;

        protected virtual void Awake()
        {
            Car = GetComponentInParent<CarComponentLinks>();
            _currentCooldown = Data.Cooldown;
        }
        
        protected void Update()
        {
            if (GameSession.Instance.State != GameState.InProgress)
            {
                return;
            }
            if (_currentCooldown > 0)
            {
                _currentCooldown -= Time.deltaTime;
            }
        }

        public abstract void Use();
    }
}