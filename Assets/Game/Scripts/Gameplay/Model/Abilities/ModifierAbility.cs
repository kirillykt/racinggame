using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class ModifierAbility : CarAbility
    {
        public override void Use()
        {
            var data = (ModifierAbilityData)Data;
            Car.ModifiersBehaviour.ActivateModifier(data.ModifierData.ID);
        }
    }
}