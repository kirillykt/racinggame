using System;
using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class ProjectileAbility : CarAbility
    {
        [SerializeField] private GameObject _projectilePrefab = null;
        [SerializeField] private Transform _spawn = null;
        private Transform _projectilesParent = null;

        private void Start()
        {
            _projectilesParent = GameObject.FindGameObjectWithTag("ProjectilesParent").transform;
        }

        public override void Use()
        {
            var data = (ProjectileAbilityData) _carAbilityData;
            var projectileInstance = Instantiate(_projectilePrefab, _spawn.position, _spawn.rotation);
            projectileInstance.GetComponent<ProjectileModel>().Init(this);
            projectileInstance.transform.parent = _projectilesParent;
            projectileInstance.GetComponent<Rigidbody>().AddForce(projectileInstance.transform.forward * data.ProjectilePushForce);
            _currentCooldown = _carAbilityData.Cooldown;
        }
    }
}