using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace CG.Gameplay.Model
{
    public class CarMotor : MonoBehaviour
    {
        [SerializeField] private CarModelData _carModelData = null;
        [SerializeField] private List<WheelCollider> _wheelColliders = new List<WheelCollider>();
        private Rigidbody _rigidbody;
        private CarComponentLinks _carComponentLinks;
        public Vector3 DesiredDirection { get; set; }
        public Vector3 LastGroundedPosition { get; private set; }

        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _carComponentLinks = GetComponent<CarComponentLinks>();
        }

        private void FixedUpdate()
        {
            if (GameSession.Instance.State != GameState.InProgress)
            {
                return;
            }
            var someWheelGrounded = false;
            foreach (var wheelCollider in _wheelColliders)
            {
                if (wheelCollider.isGrounded)
                {
                    someWheelGrounded = true;
                }
            }

            if (!someWheelGrounded)
            {
                return;
            }

            LastGroundedPosition = transform.position;
            var forward = DesiredDirection.z;
            var horizontal = DesiredDirection.x;
            ProcessForwardMovement(forward);
            ProcessHorizontalMovement(forward, horizontal);
        }

        private void ProcessForwardMovement(float forward)
        {
            if (forward > 0)
            {
                if (_rigidbody.velocity.magnitude < GetMaxVelocity())
                {
                    _rigidbody.velocity += transform.forward * 
                                           (GetAccelerationForce() * Time.fixedDeltaTime);
                }
            }
            if (forward < 0)
            {
                var velocity = _rigidbody.velocity;
                if (Vector3.Dot(velocity, transform.forward) > 0 || _rigidbody.velocity.magnitude < _carModelData.MaxBackwardsVelocity)
                {
                    _rigidbody.velocity -= transform.forward * 
                                           (_carModelData.BackwardsAccelerationForce * Time.fixedDeltaTime);
                }
            }
        }

        private void ProcessHorizontalMovement(float forward, float horizontal)
        {
            var localVelocity = transform.InverseTransformVector(_rigidbody.velocity);
            var isBraking = forward < 0 && localVelocity.z < 0;
            var angularVelocity = _rigidbody.angularVelocity;
            angularVelocity.y = Mathf.MoveTowards(angularVelocity.y,
                horizontal * _carModelData.MaxRotationSpeed,
                Time.fixedDeltaTime * _carModelData.RotationAccelerationSpeed);
            _rigidbody.angularVelocity = angularVelocity;
            var velocitySteering = 25f;
            var turningPower = isBraking ? -horizontal : horizontal;
            _rigidbody.velocity = Quaternion.AngleAxis(turningPower * Mathf.Sign(localVelocity.z)
                                                                    * velocitySteering * Time.fixedDeltaTime, transform.up) * _rigidbody.velocity;
        }

        private float GetMaxVelocity()
        {
            return _carModelData.MaxVelocity + _carComponentLinks.ModifiersBehaviour.GetAdditionalMaxVelocity();
        }

        private float GetAccelerationForce()
        {
            return _carModelData.AccelerationForce + _carComponentLinks.ModifiersBehaviour.GetAdditionalAcceleration();
        }
    }
}