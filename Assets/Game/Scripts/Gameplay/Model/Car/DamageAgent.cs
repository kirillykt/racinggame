using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class DamageAgent : MonoBehaviour
    {
        private CarComponentLinks _carComponentLinks;
        [SerializeField] private int _remainHP;
        public int RemainHP => _remainHP;
        public event Action OnDied;
        public event Action OnResurrect;
        public bool IsAlive => RemainHP > 0;
        private void Awake()
        {
            _carComponentLinks = GetComponent<CarComponentLinks>();
        }

        private void Start()
        {
            _remainHP = _carComponentLinks.CarModelData.FullHP;
            DamageSystem.Instance.Register(this);
        }

        public void ReceiveDamage(int damage)
        {
            _remainHP -= damage;
            if (_remainHP <= 0)
            {
                Die();
            }
        }

        private void Die()
        {
            OnDied?.Invoke();
            StartCoroutine(Resurrect());
        }

        private IEnumerator Resurrect()
        {
            yield return new WaitForSeconds(GameSession.Instance.GameRules.RespawnTime);
            _remainHP = _carComponentLinks.CarModelData.FullHP;
            OnResurrect?.Invoke();
        }
    }
}