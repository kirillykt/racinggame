using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class CarController : MonoBehaviour
    {
        protected CarComponentLinks _carComponentLinks;
        protected CarMotor _carMotor;
        public static CarController My { get; set; }
        public CarComponentLinks CarComponentLinks => _carComponentLinks;

        private void Awake()
        {
            _carMotor = GetComponent<CarMotor>();
            _carComponentLinks = GetComponent<CarComponentLinks>();
        }

    }
}