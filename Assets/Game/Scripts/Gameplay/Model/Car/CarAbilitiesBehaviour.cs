using System.Collections;
using System.Collections.Generic;
using CG.Data;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class CarAbilitiesBehaviour : MonoBehaviour
    {
        [SerializeField] private List<CarAbility> _carAbilities = null;
        private CarComponentLinks _carComponentLinks;
        private DamageAgent _damageAgent = null;
        private CarAbility MainAbility;
        private CarAbility SecondAbility;
        private void Start()
        {
            _damageAgent = GetComponent<DamageAgent>();
            _carComponentLinks = GetComponent<CarComponentLinks>();
            foreach (var ability in _carAbilities)
            {
                if (_carComponentLinks.CarModelData.MainAbility.ID.Equals(ability.Data.ID))
                {
                    ability.gameObject.SetActive(true);
                    MainAbility = ability;
                }

                if (_carComponentLinks.CarModelData.SecondAbility.ID.Equals(ability.Data.ID))
                {
                    ability.gameObject.SetActive(true);
                    SecondAbility = ability;
                }
            }
        }

        public void Use(int index)
        {
            if (GameSession.Instance.State != GameState.InProgress)
            {
                return;
            }

            if (!_damageAgent.IsAlive)
            {
                return;
            }
            if (index == 0)
            {
                UseAbility(MainAbility);
            }
            if (index == 1)
            {
                UseAbility(SecondAbility);
            }
        }

        private void UseAbility(CarAbility ability)
        {
            if (ability.CanUse)
            {
                ability.Use();
            }
        }

        public void Use(CarAbility ability)
        {
            if (ability == MainAbility)
            {
                Use(0);
            }

            if (ability == SecondAbility)
            {
                Use(1);
            }
        }

    }
}