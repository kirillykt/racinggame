using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class CarSpawnAgent : MonoBehaviour
    {
        private List<Transform> _respawns = new List<Transform>();
        private Rigidbody _rigidbody;
        private CarComponentLinks _carComponentLinks = null;
        private void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
            var spawns = GameObject.FindGameObjectsWithTag("Respawn");
            foreach (var spawn in spawns)
            {
                _respawns.Add(spawn.transform);
            }

            _carComponentLinks = GetComponent<CarComponentLinks>();
            _carComponentLinks.DamageAgent.OnResurrect += Respawn;
            _carComponentLinks.DamageAgent.OnDied += OnDied;
        }

        private void OnDied()
        {
            var respawn = FindClosestRespawn();
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
            _carComponentLinks.CarMotor.enabled = false;
            _carComponentLinks.CarAbilitiesBehaviour.enabled = false;
            _carComponentLinks.SetCollidersEnabled(false);
            _rigidbody.MovePosition(respawn.position);
            _rigidbody.MoveRotation(respawn.rotation);
            _rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            _rigidbody.isKinematic = true;
        }
        

        [ContextMenu("Respawn")]
        private void Respawn()
        {
            _carComponentLinks.CarMotor.enabled = true;
            _carComponentLinks.CarAbilitiesBehaviour.enabled = true;
            _carComponentLinks.SetCollidersEnabled(true);
            _rigidbody.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX |
                                     RigidbodyConstraints.FreezeRotationZ;
            _rigidbody.isKinematic = false;
        }

        private Transform FindClosestRespawn()
        {
            Transform closest = null;
            foreach (var respawn in _respawns)
            {
                if (closest == null)
                {
                    closest = respawn;
                    continue;
                }

                if (Vector3.Distance(closest.position, transform.position) >
                    Vector3.Distance(respawn.position, transform.position))
                {
                    closest = respawn;
                }
            }
            return closest;
        }
    }
}