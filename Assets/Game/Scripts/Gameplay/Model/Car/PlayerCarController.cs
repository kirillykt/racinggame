using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    [RequireComponent(typeof(CarMotor))]
    public class PlayerCarController : CarController
    {
        [SerializeField] private string _verticalAxis = "Vertical";
        [SerializeField] private string _horizontalAxis = "Horizontal";
        
        private void Start()
        {
            
        }

        private void FixedUpdate()
        {
            var vertical = Input.GetAxis(_verticalAxis);
            var horizontal = Input.GetAxis(_horizontalAxis);

            var inputVector = new Vector3(horizontal, 0, vertical);
            _carMotor.DesiredDirection = inputVector;
            
        }

        private void Update()
        {
            if (Input.GetButtonDown("Fire1"))
            {
                _carComponentLinks.CarAbilitiesBehaviour.Use(0);
            }

            if (Input.GetButtonDown("Fire2"))
            {
                _carComponentLinks.CarAbilitiesBehaviour.Use(1);
            }
        }
    }
}
