using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model.AI
{
    public class ShootIfEnemyInFront : AIAbilityLogic
    {

        protected override void UpdateLogic(float delta)
        {
            if (!_ability.CanUse)
            {
                return;
            }

            foreach (var enemy in _aiMemory.VisibleEnemies)
            {
                var direction = (enemy.transform.position - transform.position).normalized;
                var dotProduct = Vector3.Dot(direction, transform.forward);
                if (dotProduct > 0.75f)
                {
                    UseAbility();
                }
            }
        }
    }
}