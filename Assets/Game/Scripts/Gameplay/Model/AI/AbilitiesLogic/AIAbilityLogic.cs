using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace CG.Gameplay.Model.AI
{
    public abstract class AIAbilityLogic : MonoBehaviour
    {
        [SerializeField] private float _updateTime = 0.2f;
        [SerializeField] protected CarAbility _ability;
        protected CarAbilitiesBehaviour _carAbilitiesBehaviour;
        protected AIMemory _aiMemory;
        protected float _elapsedTime = 0f;
        
        private void Awake()
        {
            _carAbilitiesBehaviour = GetComponentInParent<CarAbilitiesBehaviour>();
            _aiMemory = this.GetComponentInHierarchy<AIMemory>(true);
            _elapsedTime = Random.Range(0f, _updateTime);
        }
        
        protected abstract void UpdateLogic(float delta);

        protected void LateUpdate()
        {
            _elapsedTime += Time.deltaTime;
            if (!(_elapsedTime >= _updateTime))
            {
                return;
            }
            _elapsedTime -= _updateTime;
            UpdateLogic(_updateTime);
        }

        protected void UseAbility()
        {
            _carAbilitiesBehaviour.Use(_ability);
        }
    }
}