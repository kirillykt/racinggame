using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model.AI
{
    public class UseWithRandomDelay : AIAbilityLogic
    {
        [SerializeField] private float _randomMin = 5f;
        [SerializeField] private float _randomMax = 10f;

        private float _currentDelay = 5f;
        private float _elapsedRandomTime = 0f;
        
        private void Start()
        {
            RandomizeCurrentDelay();
        }

        protected override void UpdateLogic(float delta)
        {
            _elapsedRandomTime += delta;
            if (_elapsedRandomTime >= _currentDelay)
            {
                _elapsedRandomTime -= _currentDelay;
                UseAbility();
                RandomizeCurrentDelay();
            }
        }

        private void RandomizeCurrentDelay()
        {
            _currentDelay = Random.Range(_randomMin, _randomMax);
        }
    }
}