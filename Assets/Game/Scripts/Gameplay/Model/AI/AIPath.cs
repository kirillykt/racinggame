using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model.AI
{
    public class AIPath : MonoBehaviour
    {
        private List<Transform> _waypoints = new List<Transform>();
        public static AIPath Instance { get; private set; }
        public List<Transform> Waypoints => _waypoints;
        private void Awake()
        {
            Instance = this;
            for (int i = 0; i < transform.childCount; i++)
            {
                _waypoints.Add(transform.GetChild(i));
            }
        }
    }
}