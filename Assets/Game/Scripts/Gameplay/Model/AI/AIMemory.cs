using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model.AI
{
    public class AIMemory : MonoBehaviour
    {
        [SerializeField] private float _updateTime = 0.3f;
        [SerializeField] private LayerMask _raycastMask;
        private DamageAgent _selfAgent = null;
        [SerializeField] private List<DamageAgent> _visibleEnemies = new List<DamageAgent>();

        public List<DamageAgent> VisibleEnemies => _visibleEnemies;
        private void OnEnable()
        {
            _selfAgent = GetComponentInParent<DamageAgent>();
            InvokeRepeating(nameof(UpdateMemory), Random.Range(0f, 0.2f), _updateTime);
        }

        private void UpdateMemory()
        {
            _visibleEnemies.Clear();
            var damageAgents = DamageSystem.Instance.DamageAgents;
            foreach (var damageAgent in damageAgents)
            {
                if (damageAgent.Equals(_selfAgent))
                {
                    continue;
                }

                var direction = damageAgent.transform.position - transform.position;
                var ray = new Ray(transform.position + transform.forward, direction);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, direction.magnitude, _raycastMask))
                {
                    if (hit.collider.GetComponentInParent<DamageAgent>() != null)
                    {
                        _visibleEnemies.Add(damageAgent);
                    }
                }
            }
        }
    }
}