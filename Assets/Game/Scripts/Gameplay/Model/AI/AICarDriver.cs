using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;

namespace CG.Gameplay.Model.AI
{
    public class AICarDriver : CarController
    {
        [SerializeField] private float _stoppingDistance = 3f;
        [SerializeField] private float _distanceToStartLerp = 15f;
        [SerializeField] private Transform _directionDebug = null;
        [SerializeField] private Transform _positionDebug = null;
        private List<Transform> _waypoints = null;
        private int _currentWaypoint;
        
        private void Start()
        {
            _carMotor = GetComponentInParent<CarMotor>();
            _waypoints = AIPath.Instance.Waypoints;
        }

        private void FixedUpdate()
        {
            var waypoint = _waypoints[_currentWaypoint];
            if (Vector3.Distance(transform.position, waypoint.position) < _stoppingDistance || IsNextWaypointCloser())
            {
                _currentWaypoint++;
                if (_currentWaypoint >= _waypoints.Count)
                {
                    _currentWaypoint = 0;
                }
                waypoint = _waypoints[_currentWaypoint];
            }

            
            var distanceToEndLerp = _stoppingDistance;
            var distance = Vector3.Distance(transform.position, waypoint.position);
            distance = Mathf.Clamp(distance, distanceToEndLerp, _distanceToStartLerp);
            var alpha = 1f - (distance - distanceToEndLerp) / (_distanceToStartLerp - distanceToEndLerp);
            var destination = Vector3.Lerp(waypoint.position, GetNextWaypoint().position, alpha);
            var desiredDirection = (destination - transform.position).normalized;
            _positionDebug.position = destination;
            var horizontal = transform.InverseTransformDirection(desiredDirection).x;
            var vertical = transform.InverseTransformDirection(desiredDirection).z;
            if (vertical < 0.7f && vertical > 0.5f)
            {
                vertical = 0;
            }
            else if (vertical < 0.5f)
            {
                vertical = -0.5f;
            }
            
            var direction = new Vector3(horizontal, 0, vertical).normalized;
            direction = AvoidWalls(direction);
            _carMotor.DesiredDirection = direction;
            _directionDebug.transform.forward = desiredDirection;
        }

        private Vector3 AvoidWalls(Vector3 desiredDirection)
        {
            if (Physics.Raycast(transform.position, transform.right * -1f, 0.5f))
            {
                return (desiredDirection + Vector3.right).normalized;
            }
            
            if (Physics.Raycast(transform.position, transform.right, 0.5f))
            {
                return (desiredDirection + Vector3.left).normalized;
            }
            return desiredDirection;
        }
        
        private bool IsNextWaypointCloser()
        {
            var next = GetNextWaypoint();
            var current = _waypoints[_currentWaypoint];
            return Vector3.Distance(transform.position, next.position) <
                   Vector3.Distance(current.position, next.position);
        }

        private Transform GetNextWaypoint()
        {
            var next = _currentWaypoint + 1;
            if (next >= _waypoints.Count)
            {
                next = 0;
            }
            return _waypoints[next];
        }
    }
}