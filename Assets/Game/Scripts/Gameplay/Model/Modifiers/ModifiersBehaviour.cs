using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class ModifiersBehaviour : MonoBehaviour
    {
        [SerializeField] private List<Modifier> _modifiers = null;
        private readonly List<Modifier> _activeModifiers = new List<Modifier>();
        private void Start()
        {
            foreach (var modifier in _modifiers)
            {
                modifier.Init(this);
            }
        }

        public void ActivateModifier(string id)
        {
            var modifier = GetById(id);
            modifier.Activate();
            _activeModifiers.Add(modifier);
        }

        public void Deactivate(string id)
        {
            var mod = GetById(id);
            _activeModifiers.Remove(mod);
        }

        private Modifier GetById(string id)
        {
            foreach (var modifier in _modifiers)
            {
                if (modifier.Data.ID.Equals(id))
                {
                    return modifier;
                }
            }
            return null;
        }

        public float GetAdditionalMaxVelocity()
        { 
            var maxVelocity = 0f;
            foreach (var activeModifier in _activeModifiers)
            {
                if (activeModifier is SpeedModifier speedModifier)
                {
                    maxVelocity += speedModifier.AdditionalMaxVelocity;
                }
            }
            return maxVelocity;
        }
        
        public float GetAdditionalAcceleration()
        { 
            var acceleration = 0f;
            foreach (var activeModifier in _activeModifiers)
            {
                if (activeModifier is SpeedModifier speedModifier)
                {
                    acceleration += speedModifier.AdditionalAcceleration;
                }
            }
            return acceleration;
        }
    }
}