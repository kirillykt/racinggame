using System;
using System.Collections;
using System.Collections.Generic;
using CG.Data.Modifiers;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class Modifier : MonoBehaviour
    {
        [SerializeField] private ModifierData _data = null;
        public ModifierData Data => _data;
        public bool IsActive { get; protected set; }
        protected float _currentDuration = 0f;
        protected ModifiersBehaviour _modifiersBehaviour;
        
        public virtual void Init(ModifiersBehaviour behaviour)
        {
            _modifiersBehaviour = behaviour;
        }

        public void Activate()
        {
            IsActive = true;
            _currentDuration = Data.Duration;
        }

        public void Deactivate()
        {
            IsActive = false;
            _modifiersBehaviour.Deactivate(Data.ID);
        }

        private void Update()
        {
            if (!IsActive)
            {
                return;
            }
            _currentDuration -= Time.deltaTime;
            if (_currentDuration <= 0)
            {
                Deactivate();
            }
        }
    }
}