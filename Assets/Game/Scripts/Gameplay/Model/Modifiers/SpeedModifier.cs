using System.Collections;
using System.Collections.Generic;
using CG.Data.Modifiers;
using CG.Gameplay.Model;
using UnityEngine;

namespace CG.Gameplay.Model
{
    public class SpeedModifier : Modifier
    {
        private SpeedModifierData _speedModifierData;
        public float AdditionalMaxVelocity => _speedModifierData.AdditionalMaxVelocity;
        public float AdditionalAcceleration => _speedModifierData.AdditionalAcceleration;

        public override void Init(ModifiersBehaviour behaviour)
        {
            base.Init(behaviour);
            _speedModifierData = (SpeedModifierData)Data;
        }
    }
}