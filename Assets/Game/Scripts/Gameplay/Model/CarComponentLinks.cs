using System.Collections;
using System.Collections.Generic;
using CG.Data;
using CG.Gameplay.Model;
using UnityEngine;

namespace CG.Gameplay
{
    public class CarComponentLinks : MonoBehaviour
    {
        [HideInInspector] public DamageAgent DamageAgent;
        [HideInInspector] public CarAbilitiesBehaviour CarAbilitiesBehaviour;
        [HideInInspector] public CarMotor CarMotor;
        [HideInInspector] public ModifiersBehaviour ModifiersBehaviour;
        public Collider[] Colliders;
        public CarModelData CarModelData;

        private void Awake()
        {
            DamageAgent = GetComponent<DamageAgent>();
            CarAbilitiesBehaviour = GetComponent<CarAbilitiesBehaviour>();
            CarMotor = GetComponent<CarMotor>();
            Colliders = GetComponentsInChildren<Collider>();
            ModifiersBehaviour = GetComponent<ModifiersBehaviour>();
        }

        public void SetCollidersEnabled(bool enabled)
        {
            foreach (var col in Colliders)
            {
                col.enabled = enabled;
            }
        }
    }
}