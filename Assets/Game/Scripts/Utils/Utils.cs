using System.Collections.Generic;
using UnityEngine;

namespace Utils
{
    public static class Extensions
    {
        public static void Shuffle<T>(this List<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                var randomElement = Random.Range(0, list.Count);
                var element = list[i];
                list[i] = list[randomElement];
                list[randomElement] = element;
            }
        }

        public static T RandomElement<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }
        
        public static T RandomElement<T>(this T[] list)
        {
            return list[Random.Range(0, list.Length)];
        }

        public static Vector3 Flat(this Vector3 vector3)
        {
            vector3.y = 0;
            return vector3;
        }

        public static Transform FindClosestTransform(IEnumerable<Transform> transforms, Vector3 position)
        {
            Transform closest = null;
            foreach (var transform in transforms)
            {
                if (closest == null)
                {
                    closest = transform;
                    continue;
                }

                if (Vector3.Distance(position, closest.position) 
                    > Vector3.Distance(position, transform.position))
                {
                    closest = transform;
                }
            }
            return closest;
        }
        
        public static T GetComponentInHierarchy<T>(this Component component, 
            bool includeInactive = false) where T : Component
        {
            return component.transform.root.GetComponentInChildren<T>(includeInactive);
        }
    }
}